<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'title',
        'body',
        "rating",
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    /*
     * DEVUELVE LA PUNTUACIÓN DEL POST
     */
    public function getPuntuacion($titulo, $cuerpo)
    {
        //REEMPLAZAMOS LOS \n
        $titulo = str_replace("\n", " ", $titulo);
        $cuerpo = str_replace("\n", " ", $cuerpo);

        $puntuacion_titulo = count(explode(' ', $titulo)) * 2;
        $puntuacion_cuerpo = count(explode(' ', $cuerpo));
        return $puntuacion_titulo + $puntuacion_cuerpo;
    }

}
