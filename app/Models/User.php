<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'city'
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    /*
     * Get the posts for the user.
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getBestPost(){
        return Post::select('id', 'rating', 'title', 'body')->where('user_id', $this->id)->orderBy('rating', 'desc')->first();
    }
}
