<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class DownloadDataController extends Controller
{
    private static $URL = "https://jsonplaceholder.typicode.com";
    private static $NUM_POSTS = 50;

    //POSTS

    /*
     * DESCARGAR E INSERTAR LOS POSTS A LA BD
     */
    public function getDescargarPosts(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => self::$URL . "/posts",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $posts = json_decode($response);

        $i = 0;
        $nuevos = 0;
        $actualizados = 0;
        foreach ($posts as $post){

            //COMPROBAR SI EXISTE EL POST
            $post_model = Post::find($post->id);
            if (isset($post_model)){
                //EXISTE
                $post_model->body = $post->body;
                $actualizados++;
            }else{
                //NO EXISTE
                $post_model = new Post();
                $post_model->id = $post->id;
                $post_model->user_id = $post->userId;
                $post_model->title = $post->title;
                $post_model->body = $post->body;
                $post_model->rating = $post_model->getPuntuacion($post_model->title, $post_model->body);
                $nuevos++;
            }
            $post_model->save();
            $i++;

            //COMPROBAMOS EL MÁXIMO DE POST
            if ($i == self::$NUM_POSTS){
                break;
            }
        }

        return response([
            "nuevos" => $nuevos,
            "actualizados" => $actualizados,
        ], 200);

    }

    //USUARIOS
    /*
     * OBTIENE LOS ID DE LOS USUARIOS QUE HAY QUE DESCARGAR Y GUARDA EL USUARIO SI NO EXISTE
     */
    public function getDescargarUsuarios(){
        //DESCARGAMOS EL ID DE USUARIO DE LOS POSTS
        $id_users_posts = Post::select('user_id')->groupBy('user_id')->get();
        $i=0;
        foreach ($id_users_posts as $user_id) {
            $user = User::find($user_id->user_id);
            if (isset($user)){
                continue;
            }

            $info_user = $this->getDescargarUsuario($user_id->user_id);
            $user = new User();
            $user->name = $info_user->name;
            $user->city = $info_user->address->city;
            $user->email = strtolower($info_user->email);
            $user->save();
        }

        return response([
            "nuevos" => $i,
        ], 200);
    }

    /*
     * DESCARGA UN USUARIO DE LA API MEDIANTE UN ID
     */
    private function getDescargarUsuario($id){
        //DESCARGAMOS UN USUARIO
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => self::$URL . "/users?id=$id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response)[0];

    }
}
