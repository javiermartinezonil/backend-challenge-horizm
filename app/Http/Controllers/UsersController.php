<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{

    /*
     * DEVUELVE LOS USUARIOS ORDENADORES POR LA MEDIA DE VALORACIONES
     */
    public function getUsers()
    {
        $users = User::join("posts", function ($join) {
            $join->on("posts.user_id", "=", "users.id");
        })
            ->select("users.*")
            ->orderBy(DB::raw("avg(posts.rating)"), "desc")
            ->groupBy("users.id")
            ->get();

        foreach ($users as $user) {
            $user->posts = $user->posts()->select('id', 'user_id', 'body', 'title')->get();
        }

        return response($users, 200);
    }
}
