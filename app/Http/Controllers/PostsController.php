<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;

class PostsController extends Controller
{

    /*
     * DEVUELVE LOS MEJORES POSTS DE CADA USUARIO
     */
    public function getPostTop(){
        $users = User::select('id', 'name')->get();
        foreach ($users as $user){
            $user->best_post = $user->getBestPost();
        }
        return response($users, 200);
    }

    /*
     * DEVUELVE UN POSTS MEDIANTE UN ID
     */
    public function getPostById($id){
        $post = Post::join("users", function($join){
                $join->on("users.id", "=", "posts.id");
            })
            ->select("posts.id", "body", "title", "name")
            ->where("posts.id", "=", $id)
            ->first();

        if (!isset($post)){
            return response([
                "message" => "Not found"
            ], 404);
        }

        return response($post, 200);
    }

}
