<?php

namespace Database\Seeders;

use App\Http\Controllers\DownloadDataController;
use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $download_data = new DownloadDataController();
        $download_data->getDescargarPosts();
    }
}
