<?php

use App\Http\Controllers\PostsApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//AUTH
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'download'], function () {
    Route::get('/posts', [App\Http\Controllers\DownloadDataController::class, 'getDescargarPosts']);
    Route::get('/users', [App\Http\Controllers\DownloadDataController::class, 'getDescargarUsuarios']);
});

Route::get('/users', [App\Http\Controllers\UsersController::class, 'getUsers']);
Route::get('/posts/top', [App\Http\Controllers\PostsController::class, 'getPostTop']);
Route::get('/posts/{id}', [App\Http\Controllers\PostsController::class, 'getPostById']);
